class _happy_database:

    def __init__(self):
        self.country_names = dict()
        self.region_sort = dict()

    # load csv information
    def load_happy(self, happy_file):
        f = open(happy_file, encoding='utf-8-sig')
        for line in f:
            line = line.rstrip()
            components = line.split(",")
            name = components[0]
            region = components[1]
            # happiness score
            ladder = float(components[2])
            # social support
            social = float(components[7])
            # life score
            life = float(components[8])
            # freedom score
            freedom = float(components[9])

            # create list of countries + data
            self.country_names[name] = [region,ladder,social,life,freedom]

            # create list of regions + country/data
            self.region_sort.setdefault(region,[]).append({name: [ladder,social,life,freedom]})
            
        f.close()

    # gets list of country names
    def get_countries(self):
        return self.country_names.keys()

    # gets specific country name
    def get_country(self,name):
        return self.country_names[name.title()]

    # gets list of regions
    def get_region(self,region):
        return self.region_sort[region.title()]

    # changes information of country
    def set_country(self,name, country_list):
        self.country_names[name.title()] = country_list
        if name not in self.country_names.keys():
            self.country_names[name] = dict()

    def delete_country(self, name):
        del(self.country_names[name])



if __name__ == "__main__":
        hdb = _happy_database()

        # load happiness data
        hdb.load_happy('happiness.csv')
        test = hdb.get_countries()

        print("Names of All countries: ", " ".join(test))
        print("")

        specificCountry = hdb.get_country('Bosnia And Herzegovina')
        print("Bosnia and Herzegovina Information: ")
        print('Region: ' + specificCountry[0])
        print('Happiness Score: ' + str(specificCountry[1]))
        print('Social score: ' + str(specificCountry[2]))
        print('Life score: ' + str(specificCountry[3]))
        print('Freedom score: ' + str(specificCountry[4]))
        print()

        region = hdb.get_region('Western Europe')
        print("All country data in Western Europe: ")
        print(region)
        print()

        newCountry = ['FantasyLand', 1, 2, 3, 4]
        hdb.set_country("TestCountry",newCountry)

        newTest = hdb.get_country("TestCountry")
        print("TestCountry information:" )
        print('Region: ' + newTest[0])
        print('Happiness Score: ' + str(newTest[1]))
        print('Social score: ' + str(newTest[2]))
        print('Life score: ' + str(newTest[3]))
        print('Freedom score: ' + str(newTest[4]))

