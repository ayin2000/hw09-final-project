
# OO API

**API Usage:** \
This API provides data on country happiness ratings. The API provides features of country names along with their region, happiness score, and variables used to calculate the happiness score. The API can provide values of life expectancy, social support and freedom index.

**API Commands:** \
load_happy: loads API data from csv file into two dictionaries: 
 - country_names: Contains all country names with corresponding data 
 - region_sort: Contains region names with list of countires in that region+data 

get_countries: gets list of all country names+data (country_names)\
get_country: gets specific country name+data \
get_region: gets region_sort list \ 
set_country: changes information of specifc country \
delete_country: deletes specific country

**Testing Usage:** \
Within ooapi folder:
python3 happiness_library.py
