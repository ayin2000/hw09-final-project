# HW09-final project

**Project Members:** bbarnacl, ayin

## Data Source
Link: [happiness_index](https://www.kaggle.com/ajaypalsinghlo/world-happiness-report-2021) \
Data is in the form of a csv file, happiness.csv. With a list of country names and their corresponding regions, happiness values, life expectancy values, social support, and other factors that affect a country's happiness. 


## Steps to Run Project
**Server Usage:** \
Step 1: Activate server \
python3 server.py \
\
Step 2: Open Website \
Open index.html in any web browser \
\
Step 3: Use Website \
Interact with website to get happiness values

## Presentation/Videos/UI

UI Website Link: \
[Website](https://ayin2000.gitlab.io/hw09-final-project/jsfrontend/index.html)

**Video Links**: \
UI Video: [Link](https://drive.google.com/file/d/1MnlVLjfzHpPXOjH1NRZGEK7gxGTyR74f/view?usp=sharing) \
Usage/Code Video: [Link](https://drive.google.com/file/d/10lgDSHQolU1Bv9R_aMGqpMT3Q3aKLJxS/view?usp=sharing)\
**Presentation Link**: [Link](https://docs.google.com/presentation/d/1HB6XOM3wKnhOtxAqukwnaVuI6BgPZO2fTxXX5g0KdAU/edit?usp=sharing)

## Scale/Complexity
**Scale**:
- Access 149 data points (Countries) (take 6 properties for each)
- Ability to add, update, and delete any data (plus reset)
- Create a front end that has three tabs for the main functions and then displays data at the bottom of page
- Create a functioning server, controller classes, OO class, front end(html and js), plus test files

**Complexity**:
- Take all user input and turn it into something that can be sent to the server
- Be able to display the information back to the user(different variations on data and dynamically adding data)
- Display output of server data with emojis based on happiness index value:)


