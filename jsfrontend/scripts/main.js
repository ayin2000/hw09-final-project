// set up submit button
var submitButton = document.getElementById('submit-button');
submitButton.onmouseup = getFormInfo;

// set up clear button
var clearButton = document.getElementById('clear-button');
clearButton.onmouseup = clearForm;

// set up delete button
var deleteButton = document.getElementById('delete-button');
deleteButton.onmouseup = doDelete;

// set up reset all button
var resetAllButton = document.getElementById('reset-all-button');
resetAllButton.onmouseup = resetAll;

// set up update button
var updateButton = document.getElementById('update-button');
updateButton.onmouseup = updateCountry;

// set up an add button
var addButton = document.getElementById('add-button');
addButton.onmouseup = addCountry;

// set up reset one button
var resetOneButton = document.getElementById('reset-one-button');
resetOneButton.onmouseup = resetOne;

function resetOne(){
  var url = 'http://localhost:51020/reset/' + document.getElementById('input-text-delete').value;
  var xhr = new XMLHttpRequest();
  xhr.open("PUT", url, true);

  xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
    } // end of onload

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    } // end of onerror

    a = {'data' : 'empty'};
    m = JSON.stringify(a);
    xhr.send(m);

    var label2 = document.getElementById('response-label-2');
    label2.innerHTML = 'Country Reset!';
}

function addCountry(){
  console.log('in addCountry');
  the_country = {'country' : document.getElementById('input-text-country-add').value,
                 'ladder score'  : document.getElementById('input-text-happyscore').value,
                 'region' : document.getElementById('input-text-region').value,
                 'social support' : document.getElementById('input-text-social').value,
                 'life expectancy' : document.getElementById('input-text-life').value,
                 'freedom' : document.getElementById('input-text-freedom').value};
  var url = "http://localhost:51020/country/";
  var xhr = new XMLHttpRequest();
  xhr.open("POST", url, true);

  xhr.onload = function(e){
    console.log('network response received' + xhr.responseText);
  } // end of onload

  // set up onerror - triggered if nw response is error response
  xhr.onerror = function(e){
    console.error(xhr.statusText);
  } // end of onerror

  console.log(the_country);
  country_str = JSON.stringify(the_country);
  xhr.send(country_str);

  var label1 = document.getElementById('response-label-1');
  label1.innerHTML = 'Country Added!';
}

function updateCountry(){
  console.log('in updateCountry');
  the_country = {'country' : document.getElementById('input-text-country-add').value,
                 'ladder score'  : document.getElementById('input-text-happyscore').value,
                 'region' : document.getElementById('input-text-region').value,
                 'social support' : document.getElementById('input-text-social').value,
                 'life expectancy' : document.getElementById('input-text-life').value,
                 'freedom' : document.getElementById('input-text-freedom').value};
  var url = "http://localhost:51020/country/" + document.getElementById('input-text-country-add').value;
  var xhr = new XMLHttpRequest();
  xhr.open("PUT", url, true);

  xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
  } // end of onload

    // set up onerror - triggered if nw response is error response
  xhr.onerror = function(e){
        console.error(xhr.statusText);
  } // end of onerror

  console.log(the_country);
  country_str = JSON.stringify(the_country);
  xhr.send(country_str);

  var label1 = document.getElementById('response-label-1');
  label1.innerHTML = 'Country Updated!';
}

function resetAll(){
  var url = 'http://localhost:51020/reset/';
  var xhr = new XMLHttpRequest();
  xhr.open("PUT", url, true);

  xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
    } // end of onload

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    } // end of onerror

    a = {'data' : 'empty'};
    m = JSON.stringify(a);
    xhr.send(m);

    var label2 = document.getElementById('response-label-2');
    label2.innerHTML = 'Countries Reset!';
}

function doDelete(){
  var deleteCountry = document.getElementById('input-text-delete').value;
  if (deleteCountry == ""){
    var url = "http://localhost:51020/country/"
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", url, true);

    xhr.onload = function(e){
          console.log('network response received' + xhr.responseText);
      } // end of onload

      // set up onerror - triggered if nw response is error response
      xhr.onerror = function(e){
          console.error(xhr.statusText);
      } // end of onerror

      xhr.send(null);

      var label2 = document.getElementById('response-label-2');
      label2.innerHTML = 'Countries Deleted!';
  }
  else{
    var url = "http://localhost:51020/country/" + deleteCountry;
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", url, true);

    xhr.onload = function(e){
          console.log('network response received' + xhr.responseText);
      } // end of onload

      // set up onerror - triggered if nw response is error response
      xhr.onerror = function(e){
          console.error(xhr.statusText);
      } // end of onerror

      xhr.send(null);

      var label2 = document.getElementById('response-label-2');
      label2.innerHTML = 'Country Deleted!';
  }
}

function clearForm(){
  var div = document.getElementById('dynamic-div');
  while (div) {
    div.parentNode.removeChild(div);
    div = document.getElementById('dynamic-div');
  }

  var label1 = document.getElementById('response-label-1');
  label1.innerHTML = ' ';
  var label2 = document.getElementById('response-label-2');
  label2.innerHTML = ' ';

}

function getFormInfo(){
  console.log('entered getFormInfo');

  // check to see if they want to select by region
  var regionSelectIndex = document.getElementById('select-region').selectedIndex;
  var regionSelect = document.getElementById('select-region').options[regionSelectIndex].value;

  // check country name
  var countryName = document.getElementById('input-text-cname').value;

  if (regionSelect != 'No Specific Region' && countryName !=""){
    // dynamically adding a label
    label_item = document.createElement("label");
    label_item.setAttribute("id", "dynamic-label" );

    var item_text = 'Cannot input country name and select a region at the same time.';
    label_item.append(item_text);

    var response_div = document.getElementById('response-div');
    response_div.appendChild(label_item);

  }
  else if (regionSelect != 'No Specific Region'){
    makeRegionNetworkCall(regionSelect);
  }
  else{
    makeCountryNetworkCall(countryName);
  }
}

function makeRegionNetworkCall(region){
  console.log('Entered makeRegionNetworkCall');

  var url = "http://localhost:51020/country/"
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);

  xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
        // do something
        updateFormWithRegion(region, xhr.responseText);
    } // end of onload

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    } // end of onerror

    xhr.send(null); // actually send req with no message body

}

function updateFormWithRegion(region, response_text){
  var response_json = JSON.parse(response_text);
  country_list = [];
  for (let country of response_json['countries']){
    if(country['region'] == region){
      country_list.push(country);
    }
  }
  button_list = [];
  button_list = checkButtons();

  for(let country of country_list){
    console.log(country);
    item_text = country['country'] + ' has a happiness rating of ' + country['ladder score'] + '.';
    if(button_list[1] != null){
      item_text += ' It has a social support score of ' + country['social support'] + '.';
    }
    if(button_list[2] != null){
      item_text += ' It has a life expectancy of ' + country['life expectancy'] + '.';
    }
    if(button_list[3] != null){
      item_text += ' It has a freedom score of ' + country['freedom'] + '.';
    }
    label_item = document.createElement("div");
    label_item.setAttribute("id", "dynamic-div" );

    label_item.append(item_text);

    label_item.innerHTML += "<br /><br />";

    var response_div = document.getElementById('response-div');
    response_div.appendChild(label_item);
  }
}

function checkButtons(){
    // sees which properties they checked off
    var regionCheck = null;
    var socialCheck = null;
    var lifeCheck = null;
    var freeCheck = null;
    if (document.getElementById('checkbox-region').checked){
      regionCheck = document.getElementById('checkbox-region').value;
    }
    if (document.getElementById('checkbox-socialsupport').checked){
      socialCheck = document.getElementById('checkbox-socialsupport').value;
    }
    if (document.getElementById('checkbox-lifeexpectancy').checked){
      lifeCheck = document.getElementById('checkbox-lifeexpectancy').value;
    }
    if (document.getElementById('checkbox-freedom').checked){
      freeCheck = document.getElementById('checkbox-freedom').value;
    }
    return [regionCheck, socialCheck, lifeCheck, freeCheck];
}

function makeCountryNetworkCall(country){
  console.log('Entered makeCountryNetworkCall');

  var url = "http://localhost:51020/country/" + country;
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);

  xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
        // do something
        updateFormWithCountry(xhr.responseText);
    } // end of onload

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    } // end of onerror

    xhr.send(null); // actually send req with no message body
}

function updateFormWithCountry(response_text){
  var country = JSON.parse(response_text);
  button_list = [];
  button_list = checkButtons();

  item_text = country['country'] + ' has a happiness rating of ' + country['ladder score'] + emojiCalculator(country['ladder score']) + '.';
  if(button_list[0] != null){
    item_text += ' It is in the region of ' + country['region'] + '.';
  }
  if(button_list[1] != null){
    item_text += ' It has a social support score of ' + country['social support'] + '.';
  }
  if(button_list[2] != null){
    item_text += ' It has a life expectancy of ' + country['life expectancy'] + '.';
  }
  if(button_list[3] != null){
    item_text += ' It has a freedom score of ' + country['freedom'] + '.';
  }
  if(country['result'] == 'error'){
    item_text = 'Invalid country input.';
  }

  label_item = document.createElement("div");
  label_item.setAttribute("id", "dynamic-div" );

  label_item.append(item_text);

  var response_div = document.getElementById('response-div');
  response_div.appendChild(label_item);

}

function emojiCalculator(score){
  if(score > 0 && score <= 4){
    return '😞';
  }
  else if(score > 4 && score <= 5){
    return '🙁';
  }
  else if(score > 5 && score <= 6){
    return '😐';
  }
  else if(score > 6 && score <= 7){
    return '🙂';
  }
  else if(score > 7){
    return '😁 ';
  }
}
