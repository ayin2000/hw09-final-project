# User Interaction
Step 1: Activate server \
Within server folder run: \
python3 server.py \
\
Step 2: Open Website \
Within jsfronend folder: \
Open index.html in any web browser \
\
Step 3: Use Website 

**Search Tab:** 
- Search by Country (User Input)
- Search by Region (Select from Dropdown Menu)
- Properties has checkboxes for specific data to be displayed
- Ouput displayed under Happiness Report header
- Clear button removes all outputs

**Update/Add Tab:** 
- Entries for new country information
- Either Update, replace country data based on name
- Add, creates new country entry in country list

**Delete/Reset:** 
- Delete + "No Input": Deletes all countries
- Delete + "Input": Deletes specifc country based on input
- Reset All: Resets country list to orginal (Before modifications)
- Reset(entered country) + "Input": Always requires user input to reset that specific country data

