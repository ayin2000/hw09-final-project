import unittest
import requests
import json

class TestCountriesIndex(unittest.TestCase):
    SITE_URL = 'http://localhost:51020' # replace with your assigned port id
    print("Testing for server: " + SITE_URL)
    COUNTRIES_URL = SITE_URL + '/country/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        c = {}
        r = requests.put(self.RESET_URL, json.dumps(c))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_countries_index_get(self):
        self.reset_data()
        r = requests.get(self.COUNTRIES_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testcountry = {}
        countries = resp['countries']
        for country in countries:
            if country['country'] == 'Finland':
                testcountry = country

        self.assertEqual(testcountry['region'], 'Western Europe')
        self.assertEqual(testcountry['ladder score'], 7.842)
        self.assertEqual(testcountry['social support'], 0.954)
        self.assertEqual(testcountry['life expectancy'], 72.000)
        self.assertEqual(testcountry['freedom'], 0.949)

    def test_countries_index_post(self):
        self.reset_data()

        c = {}
        c['country'] = 'Brandonland'
        c['region'] = 'Myregion'
        c['ladder score'] = 10
        c['social support'] = 1.0
        c['life expectancy'] = 120
        c['freedom'] = 1.0
        r = requests.post(self.COUNTRIES_URL, data = json.dumps(c))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['country'], 'Brandonland')

        r = requests.get(self.COUNTRIES_URL + str(resp['country']))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['country'], c['country'])
        self.assertEqual(resp['region'], c['region'])
        self.assertEqual(resp['ladder score'], c['ladder score'])
        self.assertEqual(resp['social support'], c['social support'])
        self.assertEqual(resp['life expectancy'], c['life expectancy'])
        self.assertEqual(resp['freedom'], c['freedom'])

    def test_countries_index_delete(self):
        self.reset_data()

        c = {}
        r = requests.delete(self.COUNTRIES_URL, data = json.dumps(c))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.COUNTRIES_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        countries = resp['countries']
        self.assertFalse(countries)

if __name__ == "__main__":
    unittest.main()
