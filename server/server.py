import cherrypy
from happiness_library import _happy_database
from country_controller import CountryController
from reset_controller import ResetController

def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    cdb = _happy_database()

    countryController = CountryController(cdb=cdb)
    resetController = ResetController(cdb=cdb)

    dispatcher.connect('country_get', '/country/:key', controller=countryController, action = 'GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('country_put', '/country/:key', controller=countryController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('country_delete', '/country/:key', controller=countryController, action = 'DELETE_KEY', conditions=dict(method=['DELETE']))
    dispatcher.connect('country_index_get', '/country/', controller=countryController, action = 'GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('country_index_post', '/country/', controller=countryController, action = 'POST_INDEX', conditions=dict(method=['POST']))
    dispatcher.connect('country_index_delete', '/country/', controller=countryController, action = 'DELETE_INDEX', conditions=dict(method=['DELETE']))

    dispatcher.connect('reset_key', '/reset/:key', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))


    # default OPTIONS handler for CORS, all direct to the same place
    dispatcher.connect('country_options', '/country/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('country_key_options', '/country/:key', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_options', '/reset/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_key_options', '/reset/:key', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

    conf = {
		'global' : {
			'server.thread_pool': 5,
			'server.socket_host' : 'localhost',
			'server.socket_port': 51020
		},
	'/': {
		'request.dispatch' : dispatcher,
        'tools.CORS.on' : True, # configuration for CORS
		}
	}

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

# class for CORS
class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

# function for CORS
def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] =  "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS) # CORS
    start_service()
