import unittest
import requests
import json

class TestReset(unittest.TestCase):

	SITE_URL = 'http://localhost:51020'
	print('Testing for server: ' + SITE_URL)
	RESET_URL = SITE_URL + '/reset/'

	def test_put_reset_index(self):
		c = {}
		r = requests.put(self.RESET_URL, json.dumps(c))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')
		r = requests.get(self.SITE_URL + '/country/')
		resp = json.loads(r.content.decode())
		countries = resp['countries']
		self.assertEqual(countries[0]['country'], 'Finland')

	def test_put_reset_key(self):
		c = {}
		r = requests.put(self.RESET_URL, json.dumps(c))

		# Change Movie Title and Genre
		country = 'Finland'
		c['country'] = 'Brandonland'
		c['region'] = 'Myregion'
		c['ladder score'] = '10'
		c['social support'] = '1.0'
		c['life expectancy'] = '120'
		c['freedom'] = '1.0'
		r = requests.put(self.SITE_URL + '/country/' + str(country), data=json.dumps(c))

		# Reset the changed movie back to original
		c = {}
		r = requests.put(self.RESET_URL + str(country), data=json.dumps(c))
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

		# Check if effective
		r = requests.get(self.SITE_URL + '/country/')
		resp = json.loads(r.content.decode())
		countries = resp['countries']
		self.assertEqual(countries[0]['country'], 'Finland')

if __name__ == '__main__':
		unittest.main()
