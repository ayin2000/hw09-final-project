import cherrypy
import re, json
import logging
from happiness_library import _happy_database

class CountryController(object):

    def __init__(self, cdb=None):
        if cdb is None:
            self.cdb = _happy_database()
        else:
            self.cdb = cdb

        self.cdb.load_happy('happiness.csv')

    # returns entire list of all countries
    # uses get_country
    def GET_INDEX(self):
        output = {'result' : 'success'}
        output['countries'] = []

        try:
            for country in self.cdb.get_countries():
                c_happy = self.cdb.get_country(country)
                dcountry = {'country' : country, 'region' : c_happy[0], 'ladder score' : c_happy[1], 'social support' : c_happy[2], 'life expectancy' : c_happy[3], 'freedom' : c_happy[4]}
                output['countries'].append(dcountry)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    # get specific country's data

    def GET_KEY(self, key):
        output = {'result' : 'success'}
        key = str(key)
        logging.debug('key read')

        try:
            country = self.cdb.get_country(key)
            if country is not None:
                output['country'] = key
                output['region'] = country[0]
                output['ladder score'] = country[1]
                output['social support'] = country[2]
                output['life expectancy'] = country[3]
                output['freedom'] = country[4]
            else:
                output['result'] = 'error'
                output['message'] = 'country not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    # updates specific country entry
    # uses set_country
    def PUT_KEY(self, key):
        output = {'result' : 'success'}
        key = str(key)

        data = json.loads(cherrypy.request.body.read().decode('utf-8'))

        try:
            country = list()
            country.append(data['region'])
            country.append(float(data['ladder score']))
            country.append(float(data['social support']))
            country.append(float(data['life expectancy']))
            country.append(float(data['freedom']))

            self.cdb.set_country(key, country)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = country

        return json.dumps(output)

    # adds new country index to country list
    def POST_INDEX(self):
        output = {'result' : 'success'}
        data = json.loads(cherrypy.request.body.read().decode('utf-8'))

        try:
            country = data['country']
            country_info = [data['region'], data['ladder score'], data['social support'], data['life expectancy'], data['freedom']]
            region_dict = {country : [data['ladder score'], data['social support'], data['life expectancy'], data['freedom']]}
            self.cdb.country_names[country] = country_info
            if data['region'] in self.cdb.region_sort:
                self.cdb.region_sort[data['region']].append(region_dict)
            else:
                self.cdb.region_sort[data['region']] = []
                self.cdb.region_sort[data['region']].append(region_dict)
            output['country'] = country
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    # delete a specific country
    # uses delete_Country
    def DELETE_KEY(self, key):
        output = {'result' : 'success'}
        key = str(key)

        try:
            self.cdb.delete_country(key)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    # deletes all countries in list
    def DELETE_INDEX(self):
        output = {'result' : 'success'}

        try:
            allCountries = list(self.cdb.get_countries())
            for country in allCountries:
                self.cdb.delete_country(country)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
