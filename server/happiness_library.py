class _happy_database:

    def __init__(self):
        self.country_names = dict()
        self.region_sort = dict()

    def load_happy(self, happy_file):
        f = open(happy_file, encoding='utf-8-sig')
        region_list = []
        for line in f:
            line = line.rstrip()
            components = line.split(",")
            name = components[0]
            region = components[1]
            # happiness score
            ladder = float(components[2])
            # social support
            social = float(components[7])
            # life score
            life = float(components[8])
            # freedom score
            freedom = float(components[9])

            self.country_names[name] = [region,ladder,social,life,freedom]

            self.region_sort.setdefault(region,[]).append({name: [ladder,social,life,freedom]})
            
        f.close()

    def get_countries(self):
        return self.country_names.keys()

    def get_country(self,name):
        return self.country_names[name.title()]

    def get_region(self,region):
        return self.region_sort[region.title()]

    def set_country(self,name, country_list):
        self.country_names[name.title()] = country_list
        if name not in self.country_names.keys():
            self.country_names[name] = dict()

    def delete_country(self, name):
        del(self.country_names[name])



if __name__ == "__main__":
        hdb = _happy_database()

        # load happiness data
        hdb.load_happy('happiness.csv')
        test = hdb.get_countries()
        print(test)

        

        movie = hdb.get_country('Bosnia And Herzegovina')
        region = hdb.get_region('Western Europe')

        print(movie)
    #    print('Region: ' + movie[0])
    #    print('Happiness Score: ' + str(movie[1]))
    #    print('Social score: ' + str(movie[2]))
    #    print('Life score: ' + str(movie[3]))
    #    print('Freedom score: ' + str(movie[4]))
