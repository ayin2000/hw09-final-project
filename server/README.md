# REST API Specification
**Port Number:**  51020 \
\
**Why use our server?:** \
Someone would want to use our server because it has the data for over a hundred countries' happiness data. The user is also able to append and modify this list. Someone would want to use our server because it has the information to be able to easily research the happiness of a country and then look at specific factors about the country and see if they affect the happiness rating. You can use all this data to see if there are trends in the properties about a country that affect its happiness. 

**Testing Usage:** \
Start server: \
Within server folder: \
python3 server.py

Run Tests: \
Within server folder: \
python3 test_reset_endpoint.py \
python3 test_countries_key.py \
python3 test_countries_index.py 

**Test file descriptions:** 
\
**test_countries_index.py**

 - Tests GET_INDEX, POST_INDEX, and DELETE_INDEX in
   country_controller.py
 - Test_countries_index_get uses a sample country, Finland and   
   determines if our server returns the correct data values for the   
   country
 - Test_countries_index_post creates a sample country to be added into  
   country and region lists and determines if the formatting is correct
 - Test_countries_index_delete removes all the countries in the server  
   country list and checks if that occurs on the server

**test_countries_key.py**

 - Tests GET_KEY, POST_KEY, and DELETE_KEY in country_controller.py
 - Test_country_get_key checks if a URL with key obtains the correct
   data values based on the key
 - Test_countries_put_key updates a country based on the key value with
   new data values
 - Test_countries_delete_key deletes a country based on its name/key and
   checks if the server country list has removed that country

**test_reset_endpoint.py**

 - Tests PUT_INDEX, and PUT_KEY in reset_controller.py
 - Test_put_reset_index resets the index of the server country list with
   its original values
 - Test_put_reste_key modifies a country’s data values with and then
   resets those values with PUT_KEY and determines if that occurs on the
   server



## REST API Table
![TABLE](RESTAPI_TABLE.jpg)
