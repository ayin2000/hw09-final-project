import cherrypy
import re, json
from happiness_library import _happy_database

class ResetController(object):
	def __init__(self, cdb=None):
		if cdb is None:
			self.cdb = _happy_database()
		else:
			self.cdb = cdb

	# resets all countries in list
	def PUT_INDEX(self):
		output = {'result' : 'success'}

		data = json.loads(cherrypy.request.body.read().decode())

		self.cdb.__init__()
		self.cdb.load_happy('happiness.csv')

		return json.dumps(output)

	# reset specific country data
	# uses get_country and set_country
	def PUT_KEY(self, key):
		output = {'result' : 'success'}
		key = str(key)

		try:
			data = json.loads(cherrypy.request.body.read().decode())

			cdbtmp = _happy_database()
			cdbtmp.load_happy('happiness.csv')

			country = cdbtmp.get_country(key)

			self.cdb.set_country(key, country)

		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)
