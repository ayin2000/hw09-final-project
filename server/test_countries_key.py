import unittest
import requests
import json

class TestCountries(unittest.TestCase):
    SITE_URL = 'http://localhost:51020' # replace with your port number and
    print("testing for server: " + SITE_URL)
    COUNTRIES_URL = SITE_URL + '/country/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        c = {}
        r = requests.put(self.RESET_URL, data = json.dumps(c))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_countries_get_key(self):
        self.reset_data()
        country = 'Finland'
        r = requests.get(self.COUNTRIES_URL + str(country))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['region'], 'Western Europe')
        self.assertEqual(resp['ladder score'], 7.842)
        self.assertEqual(resp['social support'], 0.954)
        self.assertEqual(resp['life expectancy'], 72.000)
        self.assertEqual(resp['freedom'], 0.949)

    def test_countries_put_key(self):
        self.reset_data()
        country = 'Finland'

        r = requests.get(self.COUNTRIES_URL + str(country))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['region'], 'Western Europe')
        self.assertEqual(resp['ladder score'], 7.842)
        self.assertEqual(resp['social support'], 0.954)
        self.assertEqual(resp['life expectancy'], 72.000)
        self.assertEqual(resp['freedom'], 0.949)

        c = {}
        c['region'] = 'Myregion'
        c['ladder score'] = 10
        c['social support'] = 1.0
        c['life expectancy'] = 120
        c['freedom'] = 1.0
        r = requests.put(self.COUNTRIES_URL + str(country), data = json.dumps(c))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.COUNTRIES_URL + str(country))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['region'], c['region'])
        self.assertEqual(resp['ladder score'], c['ladder score'])
        self.assertEqual(resp['social support'], c['social support'])
        self.assertEqual(resp['life expectancy'], c['life expectancy'])
        self.assertEqual(resp['freedom'], c['freedom'])

    def test_countries_delete_key(self):
        self.reset_data()
        country = 'Finland'

        c = {}
        r = requests.delete(self.COUNTRIES_URL + str(country), data = json.dumps(c))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.COUNTRIES_URL + str(country))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')

if __name__ == "__main__":
    unittest.main()
